//
//  ShapeRandomizerTests.swift
//  ShapeeTests
//
//  Created by Matus Littva on 19/05/2021.
//

import XCTest
@testable import Shapee

final class ShapeRandomizerTests: XCTestCase {
    
    private let limit = 200
    
    func test_randomShapeCoordinates_notExceedingMaxValues() {
        let sut = ShapeRandomizer(maxX: limit, maxY: limit)
        
        let shape = sut.randomShape(type: .triangle)
        
        XCTAssertLessThan(shape.x - shape.width, Double(limit))
        XCTAssertLessThan(shape.y - shape.height, Double(limit))
    }
    
    func test_randomShapeType_generatesCorrectType() {
        let sut = ShapeRandomizer(maxX: limit, maxY: limit)
        let randomShape = ShapeType.allCases.randomElement()!
        
        let shape = sut.randomShape(type: randomShape)
        
        XCTAssertEqual(shape.type, randomShape)
    }
}
