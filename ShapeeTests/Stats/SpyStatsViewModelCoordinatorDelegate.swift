//
//  SpyStatsViewModelCoordinatorDelegate.swift
//  ShapeeTests
//
//  Created by Matus Littva on 19/05/2021.
//

@testable import Shapee

final class SpyStatsViewModelCoordinatorDelegate: StatsViewModelCoordinatorDelegate {
    
    private(set) var closeCallCount = 0
    func close() {
        closeCallCount += 1
    }
    
    private(set) var removeAllCallCount = 0
    func removeAll() {
        removeAllCallCount += 1
    }
}
