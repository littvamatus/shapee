//
//  SpyStatsViewModelOutput.swift
//  ShapeeTests
//
//  Created by Matus Littva on 19/05/2021.
//

@testable import Shapee

final class SpyStatsViewModelOutput {
    
    private var output: StatsViewModelOutput
    private(set) var reloadHandlerCallCount = 0
    
    init(output: StatsViewModelOutput) {
        self.output = output
        self.output.reloadHandler = { [weak self] in
            self?.reloadHandlerCallCount += 1
        }
    }
}

