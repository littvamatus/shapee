//
//  StatsViewModelTests.swift
//  ShapeeTests
//
//  Created by Matus Littva on 19/05/2021.
//

import XCTest
@testable import Shapee

final class StatsViewModelTests: XCTestCase {
    
    private let shapeTypes = ShapeType.allCases
    
    func test_cellModel_containsAllShapeTypes() {
        let sut = makeSUT()
        
        XCTAssertEqual(sut.cellModels.count, shapeTypes.count)
    }
    
    func test_cellModel_containsCorrectValues() {
        let triangleCount: Int = .random(in: 0...10)
        let circleCount: Int = .random(in: 0...10)
        let squareCount: Int = .random(in: 0...10)
        let triangles = makeShapes(type: .triangle, count: triangleCount)
        let circles = makeShapes(type: .circle, count: circleCount)
        let squares = makeShapes(type: .square, count: squareCount)
        let sut = makeSUT(shapes: triangles + circles + squares)
        
        let cellModel: (ShapeType) -> SimpleCellModel? = { shapeType in
            sut.cellModels.first { $0.title == shapeType.rawValue.capitalized }
        }
        
        XCTAssertEqual(cellModel(.triangle)?.value, "\(triangleCount)")
        XCTAssertEqual(cellModel(.circle)?.value, "\(circleCount)")
        XCTAssertEqual(cellModel(.square)?.value, "\(squareCount)")
    }
    
    func test_afterRemoveAll_coordinatorDelegateCalled() {
        let sut = makeSUT()
        let spy = SpyStatsViewModelCoordinatorDelegate()
        sut.coordinatorDelegate = spy
        
        XCTAssertEqual(spy.removeAllCallCount, 0)
        sut.removeAll()
        
        XCTAssertEqual(spy.removeAllCallCount, 1)
    }
    
    func test_afterClose_coordinatorDelegateCalled() {
        let sut = makeSUT()
        let spy = SpyStatsViewModelCoordinatorDelegate()
        sut.coordinatorDelegate = spy
        
        XCTAssertEqual(spy.closeCallCount, 0)
        sut.close()
        
        XCTAssertEqual(spy.closeCallCount, 1)
    }
    
    func test_afterRemoveAll_reloadHandlerCalled() {
        let sut = makeSUT()
        let spy = SpyStatsViewModelOutput(output: sut)
        
        XCTAssertEqual(spy.reloadHandlerCallCount, 0)
        sut.removeAll()
        
        XCTAssertEqual(spy.reloadHandlerCallCount, 1)
    }
    
    // MARK: - Private
    
    private func makeSUT(shapes: [Shape] = []) -> StatsViewModel {
        StatsViewModel(shapes: shapes, shapeTypes: shapeTypes)
    }
    
    private func makeShapes(type: ShapeType, count: Int) -> [Shape] {
        let randomizer = ShapeRandomizer(maxX: 200, maxY: 200)
        return (0..<count).map { _ in randomizer.randomShape(type: type) }
    }
}
