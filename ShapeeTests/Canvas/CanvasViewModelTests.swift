//
//  CanvasViewModelTests.swift
//  ShapeeTests
//
//  Created by Matus Littva on 19/05/2021.
//

import XCTest
@testable import Shapee

final class CanvasViewModelTests: XCTestCase {

    private var sut: CanvasViewModel!
    private var spy: SpyCanvasViewModelOutput!
    private var spyDelegate: SpyCanvasViewModelCoordinatorDelegate!
    
    override func setUp() {
        super.setUp()
        
        sut = makeSUT()
        spy = SpyCanvasViewModelOutput(output: sut)
        spyDelegate = SpyCanvasViewModelCoordinatorDelegate()
        sut.coordinatorDelegate = spyDelegate
        
        sut.setup(width: 200, height: 200)
    }
    
    override func tearDown() {
        sut = nil
        spy = nil
        spyDelegate = nil
        
        super.tearDown()
    }
    
    // MARK: - Undo State
    
    func test_onSetup_undoIsDisabled() {
        XCTAssertFalse(spy.isUndoEnabled)
        XCTAssertEqual(spy.undoHandlerCallCount, 1)
    }
    
    func test_undoingWithoutAction_undoIsUpdated() {
        sut.undo()
        XCTAssertFalse(spy.isUndoEnabled)
        XCTAssertEqual(spy.undoHandlerCallCount, 2)
    }

    func test_undoingOneAddition_undoStateIsDisabled() {
        sut.addShape(type: .triangle)
        XCTAssertTrue(spy.isUndoEnabled)
        
        sut.undo()
        XCTAssertFalse(spy.isUndoEnabled)
    }

    func test_afterRemoveAll_undoIsEnabled() {
        sut.addShape(type: .circle)
        XCTAssertTrue(spy.isUndoEnabled)
        
        sut.removeAll()
        XCTAssertTrue(spy.isUndoEnabled)
    }
    
    func test_afterTwoAddition_oneUndoRemovesOneShape() {
        sut.addShape(type: .triangle)
        sut.addShape(type: .circle)
        XCTAssertEqual(spy.shapes.count, 2)
        
        sut.undo()
        XCTAssertEqual(spy.shapes.count, 1)
    }
    
        func test_undoingShapePosition_resetsCoordinates() {
            sut.addShape(type: .circle)
            let originalX = spy.shapes.first?.x
            let originalY = spy.shapes.first?.y
    
            sut.updateShapePosition(index: 0, x: 0, y: 0)
            sut.undo()
    
            XCTAssertEqual(spy.shapes.first?.x, originalX)
            XCTAssertEqual(spy.shapes.first?.y, originalY)
        }
    
        func test_undoingRemoveAll_restoresCorrectState() {
            sut.shapeTypes.forEach {
                sut.addShape(type: $0)
            }
            let shapes = spy.shapes
    
            XCTAssertTrue(spy.isUndoEnabled)
            sut.removeAll()
            XCTAssertTrue(spy.shapes.isEmpty)
            XCTAssertTrue(spy.isUndoEnabled)
    
            sut.undo()
            XCTAssertEqual(spy.shapes, shapes)
        }
 
    // MARK: - Redraw
    
    func test_afterAddShapeOnce_shapeIsAdded() {
        XCTAssertTrue(spy.shapes.isEmpty)
        sut.addShape(type: .triangle)
        
        XCTAssertEqual(spy.redrawHandlerCallCount, 1)
        XCTAssertEqual(spy.shapes.count, 1)
    }
    
    func test_afterAddMultipleShapes_shapesAreAdded() {
        XCTAssertTrue(spy.shapes.isEmpty)
        sut.shapeTypes.forEach {
            sut.addShape(type: $0)
        }
        XCTAssertEqual(spy.redrawHandlerCallCount, sut.shapeTypes.count)
        XCTAssertEqual(spy.shapes.count, sut.shapeTypes.count)
    }
    
    func test_undoingAddition_redrawIsCalled() {
        sut.addShape(type: .triangle)
        XCTAssertEqual(spy.redrawHandlerCallCount, 1)
        
        sut.undo()
        XCTAssertEqual(spy.redrawHandlerCallCount, 2)
    }
    
    func test_onPositionUpdate_redrawCalled() {
        sut.addShape(type: .circle)
        XCTAssertEqual(spy.redrawHandlerCallCount, 1)
        
        sut.updateShapePosition(index: 0, x: 20, y: 20)
        XCTAssertEqual(spy.redrawHandlerCallCount, 2)
    }
    
    func test_onRemoveAll_redrawCalled() {
        XCTAssertEqual(spy.redrawHandlerCallCount, 0)
        sut.removeAll()
    
        XCTAssertEqual(spy.redrawHandlerCallCount, 1)
    }
    
    // MARK: - Delegate
    
    func test_onShowStats_delegateCalled() {
        sut.showStats()
        
        XCTAssertEqual(spyDelegate.showStatsCallCount, 1)
    }
    
    func test_onShowStats_shapesPassed() {
        sut.addShape(type: .triangle)
        sut.showStats()
        
        XCTAssertEqual(spyDelegate.shapes.count, 1)
    }
    
    // MARK: - Private
    
    private func makeSUT(undoManager: UndoManager = UndoManager()) -> CanvasViewModel {
        CanvasViewModel(shapeTypes: ShapeType.allCases, undoManager: undoManager)
    }
}
