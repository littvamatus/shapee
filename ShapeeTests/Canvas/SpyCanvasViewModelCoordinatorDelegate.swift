//
//  SpyCanvasViewModelCoordinatorDelegate.swift
//  ShapeeTests
//
//  Created by Matus Littva on 20/05/2021.
//

@testable import Shapee

final class SpyCanvasViewModelCoordinatorDelegate: CanvasViewModelCoordinatorDelegate {
    
    private(set) var showStatsCallCount = 0
    private(set) var shapes: [Shape] = []
    
    func showStats(shapes: [Shape]) {
        showStatsCallCount += 1
        self.shapes = shapes
    }
}
