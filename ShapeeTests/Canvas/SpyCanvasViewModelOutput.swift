//
//  SpyCanvasViewModelOutput.swift
//  ShapeeTests
//
//  Created by Matus Littva on 19/05/2021.
//

@testable import Shapee

final class SpyCanvasViewModelOutput {    
    
    private var output: CanvasViewModelOutput
    
    init(output: CanvasViewModelOutput) {
        self.output = output
        bindUndoHandler()
        bindRedrawHandler()
    }

    private(set) var undoHandlerCallCount = 0
    private(set) var isUndoEnabled = true
    
    private func bindUndoHandler() {
        output.undoHandler = { [weak self] canUndo in
            self?.undoHandlerCallCount += 1
            self?.isUndoEnabled = canUndo
        }
    }
    
    private(set) var redrawHandlerCallCount = 0
    private(set) var shapes: [Shape] = []

    private func bindRedrawHandler() {
        output.redrawHandler = { [weak self] shapes in
            self?.redrawHandlerCallCount += 1
            self?.shapes = shapes
        }
    }
}
