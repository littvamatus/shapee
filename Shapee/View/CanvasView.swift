//
//  CanvasView.swift
//  Shapee
//
//  Created by Matus Littva on 18/05/2021.
//

import UIKit

protocol CanvasViewDelegate: AnyObject {
    func shapePositionUpdated(_ shapeView: ShapeView)
}

final class CanvasView: UIView {
    
    private var trackedView: ShapeView?
    
    weak var delegate: CanvasViewDelegate?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let point = touches.first?.location(in: self), let shapeView = hitTest(point, with: event) as? ShapeView {
            trackedView = shapeView
            bringSubviewToFront(shapeView)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let point = touches.first?.location(in: self) {
            trackedView?.center = point
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let shapeView = trackedView {
            delegate?.shapePositionUpdated(shapeView)
        }
        trackedView = nil
    }
    
    func draw(shapes: [Shape]) {
        subviews
            .compactMap { $0 as? ShapeView }
            .forEach {
                $0.removeFromSuperview()
            }
        shapes.forEach {
            addSubview(ShapeView(shape: $0))
        }
    }
}
