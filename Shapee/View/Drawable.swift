//
//  Drawable.swift
//  Shapee
//
//  Created by Matus Littva on 18/05/2021.
//

import UIKit

protocol Drawable {
    var path: UIBezierPath { get }
    var rect: CGRect { get }
    var fillColor: UIColor { get }
    var strokeColor: UIColor { get }
    var lineWidth: CGFloat { get }
}
