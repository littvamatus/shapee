//
//  ShapeView.swift
//  Shapee
//
//  Created by Matus Littva on 18/05/2021.
//

import UIKit

final class ShapeView: UIView {
    
    var shapePath: CGPath?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(shape: Drawable) {
        self.init(frame: shape.rect)
        setupLayer(shape: shape)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard let layer = layer.sublayers?.first as? CAShapeLayer, let path = layer.path else { return nil }
        return path.contains(point) ? self : nil
    }
    
    private func setupLayer(shape: Drawable) {
        let shapeLayer = CAShapeLayer()
        shapePath = shape.path.cgPath
        shapeLayer.path = shape.path.cgPath
        shapeLayer.strokeColor = shape.strokeColor.cgColor
        shapeLayer.fillColor = shape.fillColor.cgColor
        shapeLayer.lineWidth = shape.lineWidth
        
        layer.addSublayer(shapeLayer)
    }
}
