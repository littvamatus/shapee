//
//  DrawableShape.swift
//  Shapee
//
//  Created by Matus Littva on 18/05/2021.
//

import UIKit

extension Shape: Drawable {
    var rect: CGRect { CGRect(x: x, y: y, width: width, height: height) }
    var fillColor: UIColor { .color(from: backgroundColor) }
    var strokeColor: UIColor { .color(from: borderColor) }
    var lineWidth: CGFloat { CGFloat(borderWidth) }
    
    var path: UIBezierPath {
        let path: UIBezierPath
        
        switch type {
        case .triangle:
            let basePoint = CGPoint(x: width / 2, y: 0)
            path = UIBezierPath()
            path.move(to: basePoint)
            path.addLine(to: CGPoint(x: width, y: height))
            path.addLine(to: CGPoint(x: 0, y: height))
            path.addLine(to: basePoint)
            
        case .circle:
            path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: width, height: height))
            
        case .square:
            path = UIBezierPath()
            path.move(to: .zero)
            path.addLine(to: CGPoint(x: width, y: 0))
            path.addLine(to: CGPoint(x: width, y: height))
            path.addLine(to: CGPoint(x: 0, y: height))
            path.addLine(to: .zero)
        }
        return path
    }
}

private extension UIColor {
    class func color(from color: Color) -> UIColor {
        UIColor(red: CGFloat(color.red) / 255,
                green: CGFloat(color.green) / 255,
                blue: CGFloat(color.blue) / 255,
                alpha: CGFloat(color.alpha))
    }
}
