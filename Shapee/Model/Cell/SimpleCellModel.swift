//
//  SimpleCellModel.swift
//  Shapee
//
//  Created by Matus Littva on 19/05/2021.
//

import Foundation

struct SimpleCellModel {
    let title: String
    let value: String
}
