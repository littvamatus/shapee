//
//  Color.swift
//  Shapee
//
//  Created by Matus Littva on 18/05/2021.
//

import Foundation

struct Color {
    let red: Int
    let green: Int
    let blue: Int
    let alpha: Double
}
