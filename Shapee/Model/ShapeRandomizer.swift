//
//  ShapeRandomizer.swift
//  Shapee
//
//  Created by Matus Littva on 18/05/2021.
//

import Foundation

final class ShapeRandomizer {
    private let maxX: Double
    private let maxY: Double
    private let sizeScale: Double
    
    private lazy var minSize = min(maxX, maxY) / (sizeScale * 2)
    private lazy var maxSize = min(maxX, maxY) / sizeScale
    private var randomSize: Double { .random(in: minSize...maxSize) }
    
    init(maxX: Int, maxY: Int, sizeScale: Int = 4) {
        self.maxX = Double(maxX)
        self.maxY = Double(maxY)
        self.sizeScale = Double(sizeScale)
    }
    
    func randomShape(type: ShapeType) -> Shape {
        let size = randomSize
        return Shape(type: type, x: randomX(offset: size), y: randomY(offset: size), width: size, height: size, backgroundColor: randomColor())
    }
    
    // MARK: - Private
    
    private func randomX(offset: Double) -> Double {
        .random(in: 0...maxX - offset)
    }
    
    private func randomY(offset: Double) -> Double {
        .random(in: 0...maxY - offset)
    }
    
    private func randomColor(range: ClosedRange<Int> = (0...255), alpha: Double = 1) -> Color {
        Color(red: .random(in: range), green: .random(in: range), blue: .random(in: range), alpha: alpha)
    }
}
