//
//  Shape.swift
//  Shapee
//
//  Created by Matus Littva on 18/05/2021.
//

import Foundation

enum ShapeType: String, CaseIterable {
    case triangle
    case circle
    case square
}

struct Shape: Equatable {
    let id: UUID
    let type: ShapeType
    var x: Double
    var y: Double
    let width: Double
    let height: Double
    let backgroundColor: Color
    let borderColor: Color
    let borderWidth: Double
    
    init(id: UUID = UUID(),
         type: ShapeType,
         x: Double,
         y: Double,
         width: Double,
         height: Double,
         backgroundColor: Color,
         borderColor: Color = Color(red: 0, green: 0, blue: 0, alpha: 1),
         borderWidth: Double = 1) {
        self.id = id
        self.type = type
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.backgroundColor = backgroundColor
        self.borderColor = borderColor
        self.borderWidth = borderWidth
    }
    
    mutating func updatePosition(x: Double, y: Double) {
        self.x = x
        self.y = y
    }
    
    static func == (lhs: Shape, rhs: Shape) -> Bool {
        lhs.id == rhs.id
    }
}
