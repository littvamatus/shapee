//
//  AppCoordinator.swift
//  Shapee
//
//  Created by Matus Littva on 18/05/2021.
//

import UIKit

final class AppCoordinator: Coordinator {
    
    private let window: UIWindow
    private var mainCoordinator: Coordinator!
    
    init(window: UIWindow) {
        self.window = window
    }
    
    @discardableResult
    func start() -> UIViewController {
        mainCoordinator = CanvasCoordinator()
        let mainViewController = mainCoordinator.start()
        window.rootViewController = mainViewController
        window.makeKeyAndVisible()
        
        return mainViewController
    }
}
