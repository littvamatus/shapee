//
//  Coordinator.swift
//  Shapee
//
//  Created by Matus Littva on 18/05/2021.
//

import UIKit

protocol Coordinator {
    func start() -> UIViewController
}
