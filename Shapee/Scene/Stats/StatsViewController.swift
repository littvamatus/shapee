//
//  StatsViewController.swift
//  Shapee
//
//  Created by Matus Littva on 19/05/2021.
//

import UIKit

final class StatsViewController: UIViewController {
    @IBOutlet private var tableView: UITableView!
    
    private var viewModel: (StatsViewModelInput & StatsViewModelOutput)!
    
    convenience init(viewModel: StatsViewModelInput & StatsViewModelOutput) {
        self.init()
        self.viewModel = viewModel
        bind()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    // MARK: - Private
    
    private func setupView() {
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        navigationItem.leftBarButtonItem = UIBarButtonItem(systemItem: .close, primaryAction: UIAction { [weak self] _ in
            self?.viewModel.close()
        })
        navigationItem.rightBarButtonItem = UIBarButtonItem(systemItem: .trash, primaryAction: UIAction { [weak self] _ in
            self?.viewModel.removeAll()
        })
    }
    
    private func bind() {
        viewModel.reloadHandler = { [weak self] in
            self?.tableView.reloadData()
        }
    }
}

// MARK: - TableViewDataSource

extension StatsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.cellModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = viewModel.cellModels[indexPath.row]
        let cell = UITableViewCell(style: .value1, reuseIdentifier: nil)
        
        cell.textLabel?.text = model.title
        cell.detailTextLabel?.text = model.value
        
        return cell
    }
}
