//
//  StatsViewConfigurable.swift
//  Shapee
//
//  Created by Matus Littva on 19/05/2021.
//

import Foundation

protocol StatsViewModelInput {
    func close()
    func removeAll()
}

protocol StatsViewModelOutput {
    var cellModels: [SimpleCellModel] { get }
    var reloadHandler: (() -> Void)? { get set }
}
