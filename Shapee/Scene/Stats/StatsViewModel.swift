//
//  StatsViewModel.swift
//  Shapee
//
//  Created by Matus Littva on 19/05/2021.
//

import Foundation

protocol StatsViewModelCoordinatorDelegate: AnyObject {
    func close()
    func removeAll()
}

final class StatsViewModel: StatsViewModelInput, StatsViewModelOutput {
    
    private var shapes: [Shape]
    private let shapeTypes: [ShapeType]
    
    // MARK: - Output
    
    var cellModels: [SimpleCellModel] {
        shapeTypes.map { type in
            SimpleCellModel(title: type.rawValue.capitalized, value: "\(shapes.filter { $0.type == type }.count)")
        }
    }
    var reloadHandler: (() -> Void)?
    
    weak var coordinatorDelegate: StatsViewModelCoordinatorDelegate?
    
    init(shapes: [Shape], shapeTypes: [ShapeType]) {
        self.shapes = shapes
        self.shapeTypes = shapeTypes
    }
    
    // MARK: - Input
    
    func close() {
        coordinatorDelegate?.close()
    }
    
    func removeAll() {
        shapes.removeAll()
        reloadHandler?()
        coordinatorDelegate?.removeAll()
    }
}
