//
//  StatsCoordinator.swift
//  Shapee
//
//  Created by Matus Littva on 19/05/2021.
//

import UIKit

protocol StatsCoordinatorDelegate: AnyObject {
    func removeAll()
}

final class StatsCoordinator: Coordinator {
    
    private let shapeTypes: [ShapeType]
    private let shapes: [Shape]
    
    private weak var rootViewController: StatsViewController?
    
    weak var delegate: StatsCoordinatorDelegate?
    
    init(shapes: [Shape], shapeTypes: [ShapeType]) {
        self.shapes = shapes
        self.shapeTypes = shapeTypes
    }
    
    func start() -> UIViewController {
        let statsViewModel = StatsViewModel(shapes: shapes, shapeTypes: shapeTypes)
        statsViewModel.coordinatorDelegate = self
        let statsViewController = StatsViewController(viewModel: statsViewModel)
        rootViewController = statsViewController
        
        return UINavigationController(rootViewController: statsViewController)
    }
}

// MARK: - Coordinator Delegate

extension StatsCoordinator: StatsViewModelCoordinatorDelegate {
    
    func close() {
        rootViewController?.dismiss(animated: true)
    }
    
    func removeAll() {
        delegate?.removeAll()
    }
}
