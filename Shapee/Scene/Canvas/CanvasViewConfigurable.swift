//
//  CanvasConfigurable.swift
//  Shapee
//
//  Created by Matus Littva on 18/05/2021.
//

import Foundation

protocol CanvasViewModelInput {
    func setup(width: Int, height: Int)
    func showStats()
    func undo()
    func addShape(type: ShapeType)
    func updateShapePosition(index: Int, x: Double, y: Double)
    func removeAll()
}

protocol CanvasViewModelOutput {
    var shapeTypes: [ShapeType] { get }
    var redrawHandler: (([Shape]) -> Void)? { get set }
    var undoHandler: ((_ canUndo: Bool) -> Void)? { get set }
}
