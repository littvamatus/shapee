//
//  CanvasCoordinator.swift
//  Shapee
//
//  Created by Matus Littva on 18/05/2021.
//

import UIKit

final class CanvasCoordinator: Coordinator {
    
    private let shapeTypes = ShapeType.allCases
    
    private weak var rootViewController: CanvasViewController?
    private var statsCoordinator: StatsCoordinator?
    private var viewModel: CanvasViewModelInput?
    
    func start() -> UIViewController {
        let canvasViewModel = CanvasViewModel(shapeTypes: shapeTypes)
        viewModel = canvasViewModel
        canvasViewModel.coordinatorDelegate = self
        let canvasViewController = CanvasViewController(viewModel: canvasViewModel)
        rootViewController = canvasViewController
        
        return canvasViewController
    }
}

// MARK: - Coordinator Delegate

extension CanvasCoordinator: CanvasViewModelCoordinatorDelegate {
    
    func showStats(shapes: [Shape]) {
        let coordinator = StatsCoordinator(shapes: shapes, shapeTypes: shapeTypes)
        coordinator.delegate = self
        statsCoordinator = coordinator
        rootViewController?.show(coordinator.start(), sender: self)
    }
}

extension CanvasCoordinator: StatsCoordinatorDelegate {
    
    func removeAll() {
        viewModel?.removeAll()
    }
}
