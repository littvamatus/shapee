//
//  CanvasViewController.swift
//  Shapee
//
//  Created by Matus Littva on 18/05/2021.
//

import UIKit

final class CanvasViewController: UIViewController {
    
    @IBOutlet private var undoButton: CanvasButton!
    @IBOutlet private var canvasView: CanvasView!
    @IBOutlet private var buttonsStackView: UIStackView!
    
    private var viewModel: (CanvasViewModelInput & CanvasViewModelOutput)!
    private var drawableShapes: [Drawable] = []
    
    convenience init(viewModel: CanvasViewModelInput & CanvasViewModelOutput) {
        self.init()
        self.viewModel = viewModel
        bind()
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.setup(width: Int(view.frame.width), height: Int(view.frame.height))
        setupShapeButtons()
        canvasView.delegate = self
    }
    
    // MARK: - Private
    
    private func bind() {
        viewModel.redrawHandler = { [weak self] shapes in
            guard let self = self else { return }
            self.drawableShapes = shapes
            self.canvasView.draw(shapes: shapes)
        }
        
        viewModel.undoHandler = { [weak self] canUndo in
            self?.undoButton.isEnabled = canUndo
        }
    }
    
    private func setupShapeButtons() {
        viewModel.shapeTypes.forEach { type in
            let button = CanvasButton(frame: .zero, primaryAction: UIAction { [weak self] _ in
                self?.viewModel.addShape(type: type)
            })
            button.setImage(UIImage(systemName: type.rawValue), for: .normal)
            buttonsStackView.addArrangedSubview(button)
        }
    }
    
    // MARK: - Actions
    
    @IBAction private func showStats(_ sender: Any) {
        viewModel.showStats()
    }
    
    @IBAction private func undo(_ sender: Any) {
        viewModel.undo()
    }
}

// MARK: - CanvasViewDelegate

extension CanvasViewController: CanvasViewDelegate {
    func shapePositionUpdated(_ shapeView: ShapeView) {
        if let path = shapeView.shapePath, let index = drawableShapes.firstIndex(where: { $0.path.cgPath == path }) {
            viewModel.updateShapePosition(index: index, x: Double(shapeView.frame.origin.x), y: Double(shapeView.frame.origin.y))
        }
    }
}
