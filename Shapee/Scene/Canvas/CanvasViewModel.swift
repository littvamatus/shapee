//
//  CanvasViewModel.swift
//  Shapee
//
//  Created by Matus Littva on 18/05/2021.
//

import Foundation

protocol CanvasViewModelCoordinatorDelegate: AnyObject {
    func showStats(shapes: [Shape])
}

final class CanvasViewModel: NSObject, CanvasViewModelInput, CanvasViewModelOutput {
    
    private let undoManager: UndoManager
    private var shapeRandomizer: ShapeRandomizer!
    
    private var shapes: [Shape] = []
    
    // MARK: - Output
    
    let shapeTypes: [ShapeType]
    var redrawHandler: (([Shape]) -> Void)?
    var undoHandler: ((_ canUndo: Bool) -> Void)?
    
    weak var coordinatorDelegate: CanvasViewModelCoordinatorDelegate?
    
    init(shapeTypes: [ShapeType], undoManager: UndoManager = UndoManager()) {
        self.shapeTypes = shapeTypes
        self.undoManager = undoManager
        undoManager.groupsByEvent = false
    }
    
    // MARK: - Input
    
    func setup(width: Int, height: Int) {
        shapeRandomizer = ShapeRandomizer(maxX: width, maxY: height)
        updateUndoHandler()
    }
    
    func showStats() {
        coordinatorDelegate?.showStats(shapes: shapes)
    }
    
    func undo() {
        undoManager.undo()
        redraw()
    }
    
    func addShape(type: ShapeType) {
        addShape(shapeRandomizer.randomShape(type: type))
    }
    
    func updateShapePosition(index: Int, x: Double, y: Double) {
        let originalShape = shapes[index]
        var shape = shapes[index]
        shape.updatePosition(x: x, y: y)
        
        registerUndoGroup { target in
            if let index = target.shapes.firstIndex(of: originalShape) {
                target.replaceShape(originalShape, at: index)
            }
        }
        replaceShape(shape, at: index)
        redraw()
    }
    
    func removeAll() {
        let shapesCopy = shapes
        registerUndoGroup { target in
            target.shapes = shapesCopy
        }
        shapes.removeAll()
        redraw()
    }
    
    // MARK: - Private
    
    private func replaceShape(_ shape: Shape, at index: Int) {
        shapes.remove(at: index)
        shapes.append(shape)
    }
    
    private func addShape(_ shape: Shape) {
        let originalShapes = shapes
        
        registerUndoGroup { target in
            target.shapes = originalShapes
        }
        shapes.append(shape)
        redraw()
    }
    
    private func registerUndoGroup(handler: @escaping ((CanvasViewModel) -> Void)) {
        undoManager.beginUndoGrouping()
        undoManager.registerUndo(withTarget: self, handler: handler)
        undoManager.endUndoGrouping()
    }
    
    private func redraw() {
        redrawHandler?(shapes)
        updateUndoHandler()
    }
    
    private func updateUndoHandler() {
        undoHandler?(undoManager.canUndo)
    }
}
